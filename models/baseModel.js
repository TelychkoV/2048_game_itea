function BaseModel() {
    PubSub.call(this);
}

// Object.create() is better than 'new'
BaseModel.prototype = new PubSub();
BaseModel.prototype.constructor = BaseModel;

BaseModel.prototype.clear = function() {};